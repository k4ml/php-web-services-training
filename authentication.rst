Authentication
==============
A RESTful API should be stateless. This means that request authentication should not depend on cookies or sessions. Instead, each request should come with some sort authentication credentials.

By always using SSL, the authentication credentials can be simplified to a randomly generated access token that is delivered in the user name field of HTTP Basic Auth. The great thing about this is that it's completely browser explorable - the browser will just popup a prompt asking for credentials if it receives a 401 Unauthorized status code from the server.

Example
-------
Below is an example of using HTTP Basic authentication in PHP::

    <?php
    $api_credentials = array(
        'user1' => 'abc123',
        'user2' => 'abcxyz'
    );

    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        header('WWW-Authenticate: Basic realm="My API"');
        header('HTTP/1.1 401 Unauthorized');
        exit;
    } else {
        $username = $_SERVER['PHP_AUTH_USER'];
        $password = $_SERVER['PHP_AUTH_PW'];

        if (!array_key_exists($username, $api_credentials)) {
            header('HTTP/1.1 403 Forbidden');
            exit;
        }
        if ($password != $api_credentials[$username]) {
            header('HTTP/1.1 403 Forbidden');
            exit;
        }
    }

The ``$api_credentials`` above simply store the user's credentials details such as username and password in a PHP array, hardcoded in the source code but in practice you'll store the information in database such as MySQL.
