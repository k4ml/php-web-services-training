Testing and Debugging
=====================
One advantage of using HTTP based API is there are lot of existing tools that can be used to test and debug the API endpoint. One of such tools is the ``curl`` command line application. Below are a few examples of using ``curl`` to make an API request.

Making ``GET`` request::

    curl -v -H 'Content-Type: application/json' http://localhost:4000/User/

And you'll see the following output::

    * About to connect() to localhost port 4000 (#0)
    *   Trying ::1... Connection refused
    *   Trying 127.0.0.1... connected
    * Connected to localhost (127.0.0.1) port 4000 (#0)
    * Server auth using Basic with user 'user1'
    > GET /User/kamal HTTP/1.1
    > Authorization: Basic dXNlcjE6YWJjMTIz
    > User-Agent: curl/7.21.4 (universal-apple-darwin11.0) libcurl/7.21.4 OpenSSL/0.9.8z zlib/1.2.5
    > Host: localhost:4000
    > Accept: */*
    > Content-Type: application/json
    >
    < HTTP/1.1 200 OK
    < Host: localhost:4000
    < Connection: close
    < X-Powered-By: PHP/5.4.43
    < Content-type: text/html
    <
    * Closing connection #0
    {"url":"\/User\/kamal"}

By default, ``curl`` will make a ``GET`` request but we can explicitly specify the request method::

    curl -X POST -v -H 'Content-Type: application/json' http://localhost:4000/User/
    curl -X PUT -v -H 'Content-Type: application/json' http://localhost:4000/User/
    curl -X DELETE -v -H 'Content-Type: application/json' http://localhost:4000/User/

Other than ``curl``, we can also use a GUI based HTTP client such as `Postman`_.

.. _Postman: https://www.getpostman.com/
