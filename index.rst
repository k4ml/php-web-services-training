.. PHP Web Services Basic documentation master file, created by
   sphinx-quickstart on Sun Aug 30 09:17:43 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PHP Web Services Basic's documentation!
==================================================

Contents:

.. toctree::
   :maxdepth: 2

   introduction
   data_format
   ajax
   authentication
   testing
   example

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

