Data Format / Serialization
===========================

Encoding/Decoding
-----------------
To enable seemless integration between our web services and the applications that consume it, we need to have standard data format. The most common one is to use JSON. In PHP, there are 2 main function used to read and write JSON format::

    <?php

    $user = array(
        'id' => 1,
        'username' => 'Kamal',
        'profiles' => array(
            'credits' => 100,
            'address' => 'JB',
        ),
    );

    echo json_encode($user);

Above code will produce JSON string that look like::

    {"id":1,"username":"Kamal","profiles":{"credits":100,"address":"JB"}}

Conversely, when reading data submitted using our web service endpoint, we can use ``json_decode`` function. Let say user submit to us the above JSON string, can read it as::

    <?php
    $data = file_get_contents('php://input');
    $data_json = json_decode($data, true);
    print_r($data_json);    

When user send the request, they will get back the same data::

    curl -v -d'{"id":1,"username":"Kamal","profiles":{"credits":100,"address":"JB"}}' -H 'Content-Type: application/json' http://localhost:4000/user.php
    * About to connect() to localhost port 4000 (#0)
    *   Trying ::1... Connection refused
    *   Trying 127.0.0.1... connected
    * Connected to localhost (127.0.0.1) port 4000 (#0)
    > POST /user.php HTTP/1.1
    > User-Agent: curl/7.21.4 (universal-apple-darwin11.0) libcurl/7.21.4 OpenSSL/0.9.8z zlib/1.2.5
    > Host: localhost:4000
    > Accept: */*
    > Content-Type: application/json
    > Content-Length: 69
    >
    < HTTP/1.1 200 OK
    < Host: localhost:4000
    < Connection: close
    < X-Powered-By: PHP/5.4.43
    < Content-type: text/html
    <
    Array
    (
        [id] => 1
        [username] => Kamal
        [profiles] => Array
            (
                [credits] => 100
                [address] => JB
            )

    )
    * Closing connection #0

Defining Status Code
--------------------
It's very important when building web services, all requests get a standardized status or error code. HTTP already provides rich status code to indicate various state of the request. Our REST API can use this status codes to communicate with our users. Below are among the most common use status code:-

200 OK
~~~~~~
This response code indicates that the request was successful.

201 Created
~~~~~~~~~~~
This indicates the request was successful and a resource was created. It is used to confirm success of a PUT or POST request.

400 Bad Request
~~~~~~~~~~~~~~~
The request was malformed. This happens especially with POST and PUT requests, when the data does not pass validation, or is in the wrong format.

404 Not Found
~~~~~~~~~~~~~
This response indicates that the required resource could not be found. This is generally returned to all requests which point to a URL with no corresponding resource.

401 Unauthorized
~~~~~~~~~~~~~~~~
This error indicates that you need to perform authentication before accessing the resource.

405 Method Not Allowed
~~~~~~~~~~~~~~~~~~~~~~
The HTTP method used is not supported for this resource.

409 Conflict
~~~~~~~~~~~~
This indicates a conflict. For instance, you are using a PUT request to create the same resource twice.

500 Internal Server Error
~~~~~~~~~~~~~~~~~~~~~~~~~
When all else fails; generally, a 500 response is used when processing fails due to unanticipated circumstances on the server side, which causes the server to error out.
