Introduction
============
Web services are client and server applications that communicate over the World Wide Web’s (WWW) HyperText Transfer Protocol (HTTP). As described by the World Wide Web Consortium (W3C), web services provide a standard means of interoperating between software applications running on a variety of platforms and frameworks. Web services are characterized by their great interoperability and extensibility, as well as their machine-processable descriptions, thanks to the use of XML. Web services can be combined in a loosely coupled way to achieve complex operations. Programs providing simple services can interact with each other to deliver sophisticated added-value services.

SOAP vs XMLRPC vs HTTP/REST
---------------------------
SOAP and XMLRPC are ways to expose certain operations of the application over the network. For example, if the application has a function called ``getUser($username)``, the function can be accessed using XML such as::

    <?xml version="1.0"?>
    <soap:Envelope
    xmlns:soap="http://www.w3.org/2001/12/soap-envelope"
    soap:encodingStyle="http://www.w3.org/2001/12/soap-encoding">

    <soap:Body xmlns:m="http://www.example.org/">
      <m:getUser>
        <m:username>kamal</m:username>
      </m:getUser>
    </soap:Body>

    </soap:Envelope>

On the server side, to expose the above function, we also need to write another XML in a format called WSDL (Web Services Description Language). The file will be more verbose and details than above and most of the time, not written by hand but instead being generated with a special program that scan our application source code.

REST on the other hand is a way to expose data (called Resource) over the web using HTTP. For example, we may have a resource called ``User``, exposed through HTTP as::

    http://www.example.org/User

Having above resource, we can use standard HTTP method to do operations such as::

    GET http://www.example.org/User/kamal
    POST http://www.example.org/User
    DELETE http://www.example.org/User/kamal

Again, this is the major difference between SOAP and REST:-

* SOAP exposed a set of operations.
* REST exposed a set of resources (data).

HTTP/REST
---------
The key principles of REST involve separating your API into logical resources. These resources are manipulated using HTTP requests where the method (GET, POST, PUT, PATCH, DELETE) has specific meaning.

But what can be a resource ? Well, these should be nouns (not verbs!) that make sense from the perspective of the API consumer. Although your internal models may map neatly to resources, it isn't necessarily a one-to-one mapping. The key here is to not leak irrelevant implementation details out to your API!

Once you have your resources defined, you need to identify what actions apply to them and how those would map to your API. RESTful principles provide strategies to handle CRUD actions using HTTP methods mapped as follows:

* GET /tickets - Retrieves a list of tickets
* GET /tickets/12 - Retrieves a specific ticket
* POST /tickets - Creates a new ticket
* PUT /tickets/12 - Updates ticket #12
* PATCH /tickets/12 - Partially updates ticket #12
* DELETE /tickets/12 - Deletes ticket #12

The great thing about REST is that you're leveraging existing HTTP methods to implement significant functionality on just a single /tickets endpoint.

Security - Use SSL
------------------
Always use SSL. No exceptions. Today, your web APIs can get accessed from anywhere there is internet (like libraries, coffee shops, airports among others). Not all of these are secure. Many don't encrypt communications at all, allowing for easy eavesdropping or impersonation if authentication credentials are hijacked.

Another advantage of always using SSL is that guaranteed encrypted communications simplifies authentication efforts - you can get away with simple access tokens instead of having to sign each API request.
