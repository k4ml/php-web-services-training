<?php

class User {
    public function do_GET($params) {
        $response = array(
            'url' => $_SERVER['REQUEST_URI'],
        );
        print_r($params);
        echo json_encode($response);
    }

    public function do_POST($params) {
        $data = file_get_contents('php://input');
        $data_json = json_decode($data, true);
        print_r($params);
        echo json_encode($data_json);
    }
}

